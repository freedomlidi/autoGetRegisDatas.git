import csv


def write_csv(row_list):
    """
    数据写入csv文件
    :return:
    """
    with open('regisData.csv', 'a', newline='') as csvfile:
        writer = csv.writer(csvfile, dialect='excel')

        writer.writerow(row_list)


