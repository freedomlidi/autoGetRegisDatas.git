使用说明：

1、该脚本为在房管局官网爬取已经结束登记的信息；
2、主脚本为getRegisDatas.py、writeCsv.py为写csv文件的脚本、openCsv.bat为打开csv文件的脚本；
3、爬取的数据写入regisData.csv文件中；


注意：

1、每次运行脚本前确保关闭csv文件，否则会报异常；
